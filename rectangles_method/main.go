package main

import (
	"fmt"
	"math"
)

func main() {

	var integral1, integral2 float64
	integral1 = CalcIntegral(0, 1, 500)
	integral2 = CalcIntegral(1, 2, 500)
	fmt.Println(integral1 + integral2)
}

func InFunction(x float64) (result float64) {
	return math.Sin(x)
}

func CalcIntegral(a, b, n float64) (result float64) {
	result = 0
	var h float64
	var i float64

	h = (b - a) / n //Шаг сетки

	for i = 0; i < n; i++ {
		result += InFunction(a + h*(i+0.5)) //Вычисляем в средней точке и добавляем в сумму
	}
	result *= h

	return result
}
